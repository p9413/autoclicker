###############################
# Module for Helper functions #
###############################

import re

# remove newline
def chomp(st):
    return re.sub('[\n\r]$', '', st)