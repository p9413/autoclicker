import re

from mouse import Mouse
from keyboard import Keyboard
import utime

MOD_LEFT_CTRL   = 0xe0
MOD_LEFT_SHIFT  = 0xe1
MOD_LEFT_ALT    = 0xe2
MOD_LEFT_GUI    = 0xe3   
MOD_RIGHT_CTRL  = 0xe4
MOD_RIGHT_SHIFT = 0xe5
MOD_RIGHT_ALT   = 0xe6
MOD_RIGHT_GUI   = 0xe7

_DEKB = { '@' : [0x14, MOD_RIGHT_ALT], 'z' : [0x1c, 0], 'Z' : [0x1c, MOD_LEFT_SHIFT],
          'y' : [0x1d, 0], 'Y' : [0x1d, MOD_LEFT_SHIFT], '!' : [0x1e, MOD_LEFT_SHIFT],
          '$' : [0x21, MOD_LEFT_SHIFT], '%' : [0x22, MOD_LEFT_SHIFT], '/' : [0x24, MOD_LEFT_SHIFT],
          '{' : [0x24, MOD_RIGHT_ALT], '(' : [0x25, MOD_LEFT_SHIFT], '[' : [0x25, MOD_RIGHT_ALT],
          ')' : [0x26, MOD_LEFT_SHIFT], ']' : [0x26, MOD_RIGHT_ALT], '=' : [0x27, MOD_LEFT_SHIFT],
          '}' : [0x27, MOD_RIGHT_ALT], '?' : [0x2d, MOD_LEFT_SHIFT], "\\" : [0x2d, MOD_RIGHT_ALT],
          '+' : [0x30, 0], '*' : [0x30, MOD_LEFT_SHIFT], '~' : [0x30, MOD_RIGHT_ALT],
          '#' : [0x31, 0], "'" : [0x31, MOD_LEFT_SHIFT], '#' : [0x32, 0], "'" : [0x32, MOD_LEFT_SHIFT],
          ',' : [0x36, 0], ';' : [0x36, MOD_LEFT_SHIFT], '.' : [0x37, 0], ':' : [0x37, MOD_LEFT_SHIFT],
          '-' : [0x38, 0], '_' : [0x38, MOD_LEFT_SHIFT], '|' : [0x64, MOD_RIGHT_ALT],
        }

_ENKB = { '!' : [0x1e, MOD_LEFT_SHIFT], '@' : [0x1f, MOD_LEFT_SHIFT], '$' : [0x21, MOD_LEFT_SHIFT],
          '%' : [0x22, MOD_LEFT_SHIFT], '{' : [0x2f, MOD_LEFT_SHIFT], '/' : [0x38, 0],
          '(' : [0x26, MOD_LEFT_SHIFT], '[' : [0x2f, 0], ')' : [0x27, MOD_LEFT_SHIFT],
          ']' : [0x30, 0], '=' : [0x2e, 0], '}' : [0x30, MOD_LEFT_SHIFT],
          '?' : [0x38, MOD_LEFT_SHIFT], "\\" : [0x31, 0], '+' : [0x2e, MOD_LEFT_SHIFT],
          '*' : [0x25, MOD_LEFT_SHIFT], '~' : [0x32, MOD_LEFT_SHIFT], '#' : [0x20, MOD_LEFT_SHIFT],
          "'" : [0x34, 0], ',' : [0x36, 0], ';' : [0x33, 0], '.' : [0x37, 0],
          ':' : [0x33, MOD_LEFT_SHIFT], '-' : [0x2d, 0], '_' : [0x2d, MOD_LEFT_SHIFT],
          '|' : [0x31, MOD_LEFT_SHIFT],
         }

# send a key-code via USB HID
def send_key(keyb, mod, key):
    keyb.press(mod,key)
    keyb.release(mod, key)

# send a whole string via USB HID
def send_str(keyb, st, CHARMAP = _DEKB):
    for char in st:
        if char in CHARMAP:
            send_key(keyb, CHARMAP[char][1], CHARMAP[char][0])
        else:
            if re.search('[a-z]', char):
                send_key(keyb, 0, 0x04 + ord(char) - ord('a'))
            if re.search('[A-Z]', char):
                send_key(keyb, MOD_LEFT_SHIFT, 0x04 + ord(char) - ord('A'))
            if char == '0':
                send_key(keyb, 0, 0x27)
            if re.search('[1-9]', char):
                send_key(keyb, 0, 0x1e + ord(char) - ord('1'))

mouse = Mouse()
keyboard = Keyboard()