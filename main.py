######################
# Main Script entry  #
######################

from picolcd114 import UI
import uasyncio as asyncio

# Initialize UI
ui = UI()

# Main Loop
asyncio.run(ui.run())