##############################################
# UI Module for Output via the Pico LCD 1.14 #
##############################################

import machine
import lcdclass
import re
import time
import math
import sys
import uasyncio as asyncio
from mouse_keyboard import *
from scripts import scripts

class UI:
    MAX_LINES = 5
    pos = 0
    page = 0
    layer = ''
    items = []
    current_task = None
    
    def __init__(self):
        # LCD init
        self.pwm = machine.PWM(machine.Pin(13))
        self.pwm.freq(1000)#Screen refresh rate
        self.pwm.duty_u16(32000)#Screen Brightness, max 65535
        self.LCD = lcdclass.LCD_1inch14()

        # Keys init
        self.key_A = machine.Pin(15, machine.Pin.IN, machine.Pin.PULL_UP)
        self.key_B = machine.Pin(17, machine.Pin.IN, machine.Pin.PULL_UP)

        self.key_up = machine.Pin(2, machine.Pin.IN, machine.Pin.PULL_UP)
        self.key_mid = machine.Pin(3, machine.Pin.IN, machine.Pin.PULL_UP)
        self.key_left = machine.Pin(16, machine.Pin.IN, machine.Pin.PULL_UP)
        self.key_down = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)
        self.key_right = machine.Pin(20, machine.Pin.IN, machine.Pin.PULL_UP)

    # Translate line number to Y px
    @staticmethod
    def line_to_px(line):
        return 30 + line * 18

    # print a list
    def print_list(self, header, items):
        
        self.LCD.fill(self.LCD.white)
        self.LCD.text(header, 8, 10, self.LCD.blue)
        
        line = 0
        out = []
        
        for x in range(self.page * 5, min(self.page * 5 + 5, len(items))):
            item = items[x]
            if(line < self.page * 5 and line > (self.page * 5 + 3)):
                next
            out.append(item)
            
            self.LCD.text(item, 20, self.line_to_px(line), self.LCD.blue)
            
            line += 1
        
        self.LCD.fill_rect(10, self.line_to_px(self.pos) + 1, 5, 5, self.LCD.red)
        self.LCD.text(f"Page: {self.page + 1} / {self.page_count()}", 10, self.line_to_px(5), self.LCD.blue)
        self.LCD.show()
        return out

    # read a key press & release
    def get_key(self):
        key = ''
        if(self.key_A.value() == 0):
            key = 'A'
            button = self.key_A
        if(self.key_B.value() == 0):
            key = 'B'
            button = self.key_B
        if(self.key_up.value() == 0):
            key = 'Up'
            button = self.key_up
        if(self.key_down.value() == 0):
            key = 'Down'
            button = self.key_down
        if(self.key_left.value() == 0):
            key = 'Left'
            button = self.key_left
        if(self.key_right.value() == 0):
            key = 'Right'
            button = self.key_right
        if(self.key_mid.value() == 0):
            key = 'Mid'
            button = self.key_mid
        if(key != ''):
            wait_for_key_stop = True
            while wait_for_key_stop:
                if(button.value() == 1):
                    wait_for_key_stop = False

        return key            

    # highest page
    def page_count(self):
        return math.ceil(len(self.items) / self.MAX_LINES)

    # general navigation handling
    def navigate(self, shown_items, key):
        # Scroll
        if(key == 'Down'):
            if(self.pos < len(shown_items) - 1 and self.pos < self.MAX_LINES - 1):
                self.pos += 1
                key = ''
            elif(self.page < self.page_count() - 1):
                self.pos = 0
                self.page += 1
                key = ''
            elif(self.page == self.page_count() - 1 and self.pos == len(shown_items) - 1):
                 self.pos = 0
                 self.page = 0
                 key = ''

        if(key == 'Up'):
            if self.pos > 0:
                self.pos -= 1
                key = ''
            elif(self.page > 0):
                self.pos = self.MAX_LINES - 1
                self.page -= 1
                key = ''
            elif(self.pos == 0 and self.page == 0):
                self.pos = (len(self.items) -1 ) % self.MAX_LINES
                self.page = self.page_count() - 1
        
        return key

    # Main UI
    async def run(self):
        self.layer = '/'
        self.pos = 0
        self.page = 0
        self.current_task = None

        while True:            
            self.items = list(scripts.keys())
            
            header = 'Status: '
            if self.current_task:
                header += 'Running'
            else:
                header += 'Stopped'
            
            shown_files = self.print_list(header, self.items)
            
            key = self.get_key()
            
            if not(self.current_task):
                key = self.navigate(shown_files, key)
                    
            # run script
            if(key == 'A'):
                if not(self.current_task):
                    self.current_task = asyncio.create_task(scripts[shown_files[self.pos]].run(mouse, keyboard))
                    
            # stop script
            if(key == 'B'):
                if self.current_task:
                    self.current_task.cancel()
                    self.current_task = None
                    
            await asyncio.sleep(0.1)