import uasyncio as asyncio
from mouse_keyboard import send_str

# use a unique class name for the script, needs to match filename
class prospect_8:
    # name defines the name of the script
    name = 'Prospect Saronite (8)'
    # desc is a description of what the script does, currently unused
    #desc = 'blubb'

    # run will be triggered by main loop, takes mouse and keyboard as input
    async def run(mouse, keyboard):
        try:
            while True:
                # do your stuff here:
                send_str(keyboard, "8")
                await asyncio.sleep(1)
        except asyncio.CancelledError:
            # handle something if neccessary on task cancel:
            print('Trapped cancelled error.')
            raise
        finally:
            # Usual way to do cleanup if script has an end
            print('Cancelled - finally')