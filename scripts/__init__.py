import os

# list of all scripts
scripts = {}

# scan dir for scripts
for module in os.listdir(__path__):
    # ignore this file
    if module == '__init__.py' or module[-3:] != '.py':
        continue
    
    # import the class
    name = module[:-3]
    mod = getattr(__import__('/scripts/' + name, globals(), locals(), [name]), name)
    if hasattr(mod, 'name'):
        name = mod.name 
    scripts[name] = mod

del module