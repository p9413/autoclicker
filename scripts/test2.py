import uasyncio as asyncio

# use a unique class name for the script, needs to match filename
class test2:
    # name defines the name of the script
    name = 'test'
    # desc is a description of what the script does, currently unused
    #desc = 'blubb'

    # run will be triggered by main loop, takes mouse and keyboard as input
    async def run(mouse, keyboard):
        try:
            while True:
                # do your stuff here:
                print('test')
                #    send_str(keyboard, "8")
                await asyncio.sleep(1)
        except asyncio.CancelledError:
            # handle something if neccessary on task cancel:
            print('Trapped cancelled error.')
            raise
        finally:
            # Usual way to do cleanup if script has an end
            print('Cancelled - finally')